This repository contains a few random presentations. It is likely to
grow in the future.

All presentations in this repository are Copyright (c) 2019 Johan
Liseborn.

All the presentations in this repository are made available under the
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

Code examples in this repository are made available under the
[GNU General Public License, version 3](https://www.gnu.org/licenses/gpl-3.0.html), if
not noted otherwise.

![alt text](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons License") ![alt text](https://www.gnu.org/graphics/gplv3-with-text-84x42.png "GPLv3")
