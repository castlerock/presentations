{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MicroPython on a BBC micro:bit\n",
    "\n",
    "## Johan Liseborn, Castlerock AB\n",
    "\n",
    "### johan@castlerock.se\n",
    "\n",
    "I grew up during the home computer revolution. The first thing I programmed was a ZX81, and I got my own VIC-20 in 1981; I then owned a C64, some Ataris, a Sinclair QL, before I turned to a 486 running Linux in the early 90s.\n",
    "\n",
    "I have been working with a lot of different types of systems, primarily backend. A few years ago I got into embedded systems and IoT, which is a little bit like being thrown back to those early days, even though the microcontrollers of today are far more powerful than my faithful C64.\n",
    "\n",
    "NOTE: Most of the code examples in this notebook are taken more or less verbatim from documentation of the MicroPython implementation for the BBC micro:bit (https://microbit-micropython.readthedocs.io/en/latest/), which has a separate copyright and licensing than the rest of the presentation material.\n",
    "\n",
    "## What is an Embedded System?\n",
    "\n",
    "Wikipedia (https://en.wikipedia.org/wiki/Embedded_system):\n",
    "\n",
    "> An embedded system is a controller with a dedicated function within a larger mechanical or electrical system, often with real-time computing constraints. It is embedded as part of a complete device often including hardware and mechanical parts. Embedded systems control many devices in common use today. Ninety-eight percent of all microprocessors manufactured are used in embedded systems.\n",
    "\n",
    "A general purpose computer usually have a CPU and a number of supporting ICs for handling different resources, such as memory, graphics, sound, storage, serial communication, networking, etc.\n",
    "\n",
    "An embedded system is usually built around a *microcontroller* (MCU), which is essentially a computer shrunk onto one single chip. It usually contains things like RAM, FLASH, general purpose I/O, ADC/DAC, different types of communication buses, such as UART, I2C, and SPI. Sometimes they also include things like Bluetooth/BLE, WiFi, or other radio technologies.\n",
    "\n",
    "Many moons ago, microcontrollers used to be 8 bit circuits. Then came 16 bit, and nowadays, 32 bit cores are increasingly common. A very popular architecture is the ARM Cortex-M series. There is also a fully open processor core definition, called RISC-V.\n",
    "\n",
    "Traditionally, embedded systems has been programmed in C and assembler.\n",
    "\n",
    "## What is MicroPython?\n",
    "\n",
    "* Baremetal re-implementation of Python 3\n",
    "* Targets microcontrollers and embedded systems\n",
    "* Implements large parts of CPython\n",
    "* Support for some of the standard library\n",
    "\n",
    "### Some differences from CPython\n",
    "\n",
    "* Method Resolution Order (also impacts calls to super())\n",
    "* MicroPython counts self as an argument\n",
    "\n",
    "### Some Boards\n",
    "\n",
    "* Adafruit Feather M0, M4 Express\n",
    "* Adafruit Circuit Playground (Cortex-M4)\n",
    "* Adafruit HUZZAH (ESP8266, Tensilica Xtensa)\n",
    "* Adafruit HUZZAH32 (ESP32, Tensilica Xtensa)\n",
    "* PyBoard (STM32F405RG, Cortex-M4)\n",
    "* PyCom WiPy, SiPy, LoPy, GPy, FiPy (CC3200, ESP32)\n",
    "* micro:bit (Cortex-M0)\n",
    "\n",
    "Cost is between about 15 to 50 dollars and upwards.\n",
    "\n",
    "## What is the BBC micro:bit?\n",
    "\n",
    "Wikipedia:\n",
    "\n",
    ">The Micro Bit (also referred to as BBC Micro Bit, stylized as micro:bit) is an open source hardware ARM-based embedded system designed by the BBC for use in computer education in the UK. It was first announced on the launch of BBC's Make It Digital campaign on 12 March 2015 with the intent of delivering 1 million devices to pupils in the UK. The final device design and features were unveiled on 6 July 2015 whereas actual delivery of devices, after some delay, began in February 2016.\n",
    ">\n",
    ">The device is described as half the size of a credit card and has an ARM Cortex-M0 processor, accelerometer and magnetometer sensors, Bluetooth and USB connectivity, a display consisting of 25 LEDs, two programmable buttons, and can be powered by either USB or an external battery pack. The device inputs and outputs are through five ring connectors that form part of a larger 25-pin edge connector. \n",
    "\n",
    "* Nordic nRF51822 – 16 MHz 32-bit ARM Cortex-M0 microcontroller, 256 KB flash memory, 16 KB static ram, 2.4 GHz Bluetooth low energy wireless networking. The ARM core has the capability to switch between 16 MHz or 32.768 kHz.\n",
    "* NXP/Freescale KL26Z – 48 MHz ARM Cortex-M0+ core microcontroller, that includes a full-speed USB 2.0 On-The-Go (OTG) controller, used as a communication interface between USB and main Nordic microcontroller. This device also performs the voltage regulation from the USB supply (4.5-5.25V) down to the nominal 3.3 volts used by the rest of the PCB. When running on batteries this regulator is not used.\n",
    "* NXP/Freescale MMA8652 – 3-axis accelerometer sensor via I²C-bus.\n",
    "* NXP/Freescale MAG3110 – 3-axis magnetometer sensor via I²C-bus (to act as a compass and metal detector).\n",
    "* Temperature sensor (on the nRF51)\n",
    "* MicroUSB connector, battery connector, 25-pin edge connector.\n",
    "* Display consisting of 25 LEDs in a 5×5 array.\n",
    "* Three tactile pushbuttons (two for applications, one for reset).\n",
    "* 2-3 PWM outputs\n",
    "* 6-17 GPIO pins\n",
    "* 6 analog inputs (10 bit resolution)\n",
    "* Serial I/O (UART)\n",
    "* SPI\n",
    "* I2C\n",
    "\n",
    "## Some Links\n",
    "\n",
    "* https://www.micropython.org\n",
    "* https://www.circuitpython.org\n",
    "* https://www.espruino.com\n",
    "* https://www.adafruit.com\n",
    "* https://pycom.io/\n",
    "* https://www.sparkfun.com\n",
    "* https://www.seeedstudio.com\n",
    "* https://www.electrokit.com\n",
    "* https://www.pimoroni.com"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Examples for BBC micro:bit\n",
    "\n",
    "## Blinkenlights\n",
    "\n",
    "The embedded equivalent of `Hello, World!` is a blinking LED. The micro:bit is almost a bit like cheating, as it has a very nice display object for setting and clearing LEDs, as well as more advanced things."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "\n",
    "display.set_pixel(2, 2, 9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let us make a more fancy example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "\n",
    "while True:\n",
    "    for i in range(10):\n",
    "        display.set_pixel(2, 2, i)\n",
    "        sleep(10)\n",
    "        \n",
    "    for i in range(10):\n",
    "        display.set_pixel(2, 2, 9 - i)\n",
    "        sleep(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now let us do something even more fancy..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "\n",
    "display.scroll('Hello, World!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Temperature\n",
    "\n",
    "The nRF51 chip of the micro:bit contains a temperature sensor which will give you the approximate ambient temperature in Celsius."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from micropython import *\n",
    "\n",
    "temperature()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accelerometer\n",
    "\n",
    "The micro:bit contains a three-axis accelerometer.\n",
    "\n",
    "Let us first see how to read the axies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "\n",
    "accelerometer.get_x()\n",
    "accelerometer.get_y()\n",
    "accelerometer.get_z()\n",
    "accelerometer.get_values()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let is have a look at *gestures*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import microbit\n",
    "\n",
    "while True:\n",
    "    print(microbit.accelerometer.current_gesture())\n",
    "    sleep(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compass\n",
    "\n",
    "The micro:bit contains a magnetometer that can be used to build a very crude compass."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "\n",
    "compass.calibrate()\n",
    "\n",
    "while True:\n",
    "    sleep(100)\n",
    "    needle = ((15 - compass.heading()) // 30) % 12\n",
    "    display.show(Image.ALL_CLOCKS[needle])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Buttons\n",
    "\n",
    "The micro:bit has two programmable buttons (and one reset button). Here is a small program that will randomly show an image when you press button `A`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import microbit\n",
    "import random\n",
    "\n",
    "while True:\n",
    "    if microbit.button_a.is_pressed():\n",
    "        microbit.display.show(random.choice([Image.HAPPY, Image.SAD, Image.CONFUSED]))\n",
    "        \n",
    "    if microbit.button_b.is_pressed():\n",
    "        microbit.display.scroll('Hello, World!')\n",
    "    \n",
    "    sleep(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can get low level access to directly read memory positions on the micro:bit; sadly we can not write to memory though..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import machine\n",
    "\n",
    "machine.unique_id()\n",
    "machine.mem8[0x00]\n",
    "machine.mem16[0x00]\n",
    "machine.mem32[0x00]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can get some system level resource information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import micropython\n",
    "\n",
    "micropython.mem_info([])\n",
    "micropython.qstr_into([])\n",
    "micropython.stack_use()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Radio and Accelerometer\n",
    "\n",
    "Now let us create a more extensive example, using both radio and accelerometer. We will create an application that lets you simulate a marble on a board, where you can balance and roll the marble by tilting the board.\n",
    "\n",
    "We will then extend the example to send information on the marbles position over radio, and have other micro:bits receive this information and show the marble moving.\n",
    "\n",
    "First the code for the controller."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "import radio\n",
    "\n",
    "radio.on()\n",
    "\n",
    "x = 2\n",
    "y = 2\n",
    "sensitivity = 50\n",
    "pause = 90\n",
    "fade = 2\n",
    "\n",
    "while True:\n",
    "    roll = accelerometer.get_x()\n",
    "    yaw = accelerometer.get_y()\n",
    "\n",
    "    if roll < -sensitivity:\n",
    "        x = max(0, x - 1)\n",
    "    elif roll > sensitivity:\n",
    "        x = min(4, x + 1)\n",
    "\n",
    "    if yaw < -sensitivity:\n",
    "        y = max(0, y - 1)\n",
    "    elif yaw > sensitivity:\n",
    "        y = min(4, y + 1)\n",
    "\n",
    "    for i in range(5):\n",
    "        for j in range(5):\n",
    "            brightness = max(0, display.get_pixel(i, j) - fade)\n",
    "            display.set_pixel(i, j, brightness)\n",
    "            \n",
    "    display.set_pixel(x, y, 9)\n",
    "    radio.send('PIXEL {} {}'.format(x, y))\n",
    "    sleep(pause)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And then the code for the receiver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from microbit import *\n",
    "import radio\n",
    "\n",
    "radio.on()\n",
    "\n",
    "x = 2\n",
    "y = 2\n",
    "pause = 90\n",
    "fade = 2\n",
    "\n",
    "while True:\n",
    "    msg = radio.receive()\n",
    "\n",
    "    if msg is not None:\n",
    "        cmd = msg.split()\n",
    "\n",
    "        if cmd[0] == 'PIXEL':\n",
    "            x = int(cmd[1])\n",
    "            y = int(cmd[2])\n",
    "\n",
    "    for i in range(5):\n",
    "        for j in range(5):\n",
    "            brightness = max(0, display.get_pixel(i, j) - fade)\n",
    "            display.set_pixel(i, j, brightness)\n",
    "            \n",
    "    display.set_pixel(x, y, 9)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "CircuitPython",
   "language": "python",
   "name": "circuitpython"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "python",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "pygments_lexer": "python3",
   "version": "3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
