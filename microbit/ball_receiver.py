from microbit import *
import radio

radio.on()

x = 2
y = 2
pause = 90
fade = 2

while True:
    msg = radio.receive()

    if msg is not None:
        cmd = msg.split()

        if cmd[0] == 'PIXEL':
            x = int(cmd[1])
            y = int(cmd[2])

    for i in range(5):
        for j in range(5):
            brightness = max(0, display.get_pixel(i, j) - fade)
            display.set_pixel(i, j, brightness)
    display.set_pixel(x, y, 9)
    sleep(pause)
